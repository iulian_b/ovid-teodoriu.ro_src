<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       Cake.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {
	
	function beforeFilter() {
		parent::beforeFilter();
		$lang = $this->params['pass'];
		if (!$this->Session->check('Config.language')) {
			$lang = '';
			if ($this->Cookie->read('Ovid.lang'))
				$lang = $this->Cookie->read('Ovid.lang');
			$this->change_language($lang);
		}
		if (!empty($lang)){
			if ($lang[0] == 'eng'){
				$this->change_language('eng');
				Configure::write('Config.language', 'eng');
			}
		}else{
			$this->change_language('rum');
			Configure::write('Config.language', 'rum');
		}
	}

/**
 * Controller name
 *
 * @var string
 */
	public $name = 'Pages';

/**
 * Default helper
 *
 * @var array
 */
	public $helpers = array('Html', 'Session');

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			$this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(implode('/', $path));
	}
	
	//MY FUNCTIONS
	
	public function home(){
		//$this->layout = 'info_page';
		$this->set('page_for_layout', 'pageStudioList');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer'));
	}
	
	
	public function despre_mine(){
		$this->layout = 'info_page';
		$this->set('page_for_layout', 'pageAbout');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('About Me'));
		$this->render('about');
	}
	public function tehnica(){
		$this->layout = 'info_page';
		$this->set('page_for_layout', 'pageTehnic');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Making of'));
		$this->render('making_of');
	}
	public function nunta(){
		$this->layout = 'gallery';
		$this->set('page_for_layout', 'pageNunta');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Wedding'));
		$this->render('wedding');
	}
	public function botez(){
		$this->layout = 'gallery';
		$this->set('page_for_layout', 'pageBotez');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Baptism'));
		$this->render('baptism');
	}
	public function natura(){
		$this->layout = 'gallery';
		$this->set('page_for_layout', 'pageNatura');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Nature'));
		$this->render('nature');
	}

	
	
	
	
	
	public function about(){
        $this->layout = 'info_page';
		$this->set('page_for_layout', 'pageAbout');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('About Me'));
	}
	public function making_of(){
        $this->layout = 'info_page';
		$this->set('page_for_layout', 'pageTehnic');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Making of'));
	}
	public function wedding(){
		$this->layout = 'gallery';
		$this->set('page_for_layout', 'pageNunta');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Wedding'));
	}
	public function baptism(){
		$this->layout = 'gallery';
		$this->set('page_for_layout', 'pageBotez');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Baptism'));
	}
	public function studio_list(){
		$this->set('page_for_layout', 'pageStudioList');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Studio'));
	}
	public function studio_diana(){
		$this->layout = 'gallery';
		$this->set('page_for_layout', 'pageStudioDiana1');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Studio Outdoors'));
	}
	public function studio_diana2(){
		$this->layout = 'gallery';
		$this->set('page_for_layout', 'pageStudioDiana2');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Studio Outdoors'));
	}
	public function studio_deea(){
		$this->layout = 'gallery';
		$this->set('page_for_layout', 'pageStudioDeea');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Studio Outdoors'));
	}
	public function studio_deea_bw(){
		$this->layout = 'gallery';
		$this->set('page_for_layout', 'pageStudioDeeaBW');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Studio Outdoors Black & White'));
	}
	public function studio_anda(){
		$this->layout = 'gallery';
		$this->set('page_for_layout', 'pageStudioAnda');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Studio'));
	}
	public function studio_miha(){
		$this->layout = 'gallery';
		$this->set('page_for_layout', 'pageStudioMiha');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Studio Indoors'));
	}
	public function nature(){
		$this->layout = 'gallery';
		$this->set('page_for_layout', 'pageNatura');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Nature'));
	}
	
	public function contact(){
		$this->layout = 'info_page';
		$this->set('page_for_layout', 'pageContact');
		$this->set('title_for_layout', 'Ovid Teodoriu: '.__('cameraman &amp; photographer').' - '. __('Contact'));
		ClassRegistry::init('Contact');
		if ($this->request->is('post')) {
			
			ClassRegistry::init('Contact')->create();
			if (ClassRegistry::init('Contact')->save($this->request->data)) {
				$this->Session->setFlash(__('Message Sent. You\'re gonna be contacted as soon as possible.'));
				
				$headers = 'From: '.$this->data['Contact']['email']. "\r\n" .
										'Reply-To: '.$this->data['Contact']['email']. "\r\n" .
										'X-Mailer: PHP/' . phpversion();
										
				$message = 'Nume: '.$this->data['Contact']['name']."\r\n".
									 'Telefon: '.$this->data['Contact']['phone']."\r\n".
									 'Mesaj: '.$this->data['Contact']['message'];
				mail(
						'ovid_teodoriu@yahoo.com',
						$this->data['Contact']['subject'].' ( Mesaj venit de pe site )',
						$message,
						$headers,
						"-f".$this->data['Contact']['email']
						);

				$this->redirect(array('action' => 'about'));
			} else {
				$this->Session->setFlash(__('The message could not be sent. Plese check your inputs and try again.'));
			}
		}
	}
	
	public function change_language($language = null){
		if (!empty($language)) {
			$this->Session->write('Config.language', $language);
			$this->Cookie->write('Ovid.lang', $language, false, 3600);
		}
	}
}

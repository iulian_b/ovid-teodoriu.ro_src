$(document).ready(function(){
	if (!$('.infoPage').length) {
		var menu = document.querySelector('.pageName');
		var origOffsetY = menu.offsetTop+100;
		document.onscroll = scroll;
	}

	function scroll() {
	  if ($(window).scrollTop() > origOffsetY) {
		$('.pageName').addClass('sticky');
	  } else {
		$('.pageName').removeClass('sticky');
	  }
	}
	
	$('#bicBurgerMenu').click(function(){
		$('.bicMeniu').show();
	});
	
	$('.bicMeniu').mouseleave(function(){
		if ($(this).css('position') == 'absolute')
			$(this).hide();
	});
	
	$('.bicMeniu li').click(function(){
		if ($('.bicMenu').css('position') == 'absolute')
			$('.bicMeniu').hide();
	});
});

$(window).load(function(){
	$('#bicLoader').css('display', 'none');
	$('#content').css('display', 'block');
	bicResize();
});

window.onresize = bicResize;

function bicResize(){
	$('body, html').css('overflow', 'hidden');
	var window_width = $(window).width();
	$('body, html').css('overflow', 'visible');
	var img_width = $('.bicImageWrapper').width();
	var offset = 13;
	for (var i=5; i > 0; i--) {
		if (Math.round(window_width/i) > 300) {
			img_width = Math.round(window_width/i);
			if (i <= 2) offset -= i-3;
			else offset -= i;
			break;
		}
	}
	$('.bicImageWrapper').width(img_width-offset);
	var imgHeight = $('.bicImageWrapper img').height();
	$('.bicImageWrapper').each(function(){
		$(this).height(imgHeight);
	});
	if (window_width > 775)
		$('.bicMeniu').css('display', 'block');
	else
		$('.bicMeniu').css('display', 'none');
}

$(document).ready(function(){
    $("a.bicImageWrapper").fancybox();
    $("a.bicVideoWrapper").fancybox({
      'padding'    : 0,
      'autoScale'    : false,
      'transitionIn'  : 'none',
      'transitionOut'  : 'none',
      'title'      : this.title,
      'width'        : 600,
      'height'    : 508,
      'type'      : 'swf',
      'swf'      : {
          'wmode'    : 'transparent',
          'allowfullscreen'  : 'true'
      }
    });
});

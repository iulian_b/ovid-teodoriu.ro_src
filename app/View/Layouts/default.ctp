<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('cake.generic');
		echo $this->Html->css('bicStyle');
		echo $this->Html->css('metro');
		echo $this->Html->css('mobile');

		echo $this->Html->script('jquery-1.7.1.min');

		if ($this->params['action'] == 'about' || $this->params['action'] == 'despre_mine' || $this->params['action'] == 'contact') {
			echo $this->Html->script('coin-slider');
			echo $this->Html->script('myCoinSlider');
		}
		
		//echo $this->Html->script('mootools');
		//echo $this->Html->script('mootools-more');
		//echo $this->Html->script('MooDropMenu');
		//echo $this->Html->script('preloader');
		echo $this->Html->script('bicScripts');
	

		echo $scripts_for_layout;
	?>
	<script type="text/javascript">
	
	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-28983027-1']);
	  _gaq.push(['_trackPageview']);
	
	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();
	
	</script>
</head>
<body>
	<div id="container">
		<div id="header">
			<div id="<?php echo $page_for_layout;?>" class="pageName">
				<?php echo $this->element('header'); ?>
			</div>
		</div>
		<div id="bicLoader" style="display:block"></div>
		<!-- <div id="footer_1" style="display:block;">
			<?php echo $this->element('footer'); ?>
		</div> -->
		<div id="content" style="display:none;">
			<?php echo $this->Session->flash(); ?>
			<?php echo $content_for_layout; ?>
		</div>
		<div id="footer_2" style="display:block;">
			<?php echo $this->element('footer'); ?>
		</div>
	</div>
</body>
</html>

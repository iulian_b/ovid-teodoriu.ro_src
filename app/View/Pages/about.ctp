<div id="columnsWrapper">
	<div class="bicLeftColumn">
		<div id="bicPhoto">
			<?php echo $this->Html->image('avatar_empty.png'); ?>
			<?php echo $this->Html->image('avatar.png'); ?>
		</div>
		<!-- <div class="bicSummaryWrapper">
			<div id="bicSummary">
			</div>
		</div> -->
	</div>
	<div class="bicRightColumn">
		<div class="bicBox">
			<div class="bicTitleBox" id="bicTopTitle">
				<span class="bicTitleText"><?php echo __('About Me', true); ?></span>
			</div>
			<div id="bicAboutBox">
				<div class="bicAboutText">
					<p>&emsp;&emsp;<?php echo __('I\'m working with images, and I\'m not just doing my job. My passion goes beyond that. I\'m in a permanent competition with myself and I always think I can do more. I\'m in the photo-video businesss for about 25 years, combining theoretical knowledges with practice: fifteen years as a TV cameraman, three years of photojournalism, two years as an apprentice in a photo-lab and a three-year school for photographers. I\'m not an artist, but I like to think that I have a grain of talent... that, a lot of experience and the will to always rise to the challenge.'); ?></p>
					<p>&emsp;&emsp;<strong><?php echo __('Why should I be your cameraman and photographer?'); ?></strong></p>
					<p>&emsp;&emsp;<?php echo __('Because the event you want to keep forever is unique, and you cannot afford the luxury of taking  chances by hiring beginners, or "professionals" that aren\'t interested in what they\'re doing. Attitude and approach are essential in our profession.'); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="bicGalleryWrapper">
	<?php for($i=0; $i < 2000; $i++) { ?>
		<?php if (file_exists(WWW_ROOT.'img/botez/thumbs/IMG_'.sprintf('%04d', $i).'.jpg')) {?>
			<a class="bicImageWrapper" href='<?php echo '/img/botez/IMG_'.sprintf('%04d', $i).'.jpg'; ?>' rel='lightbox-botez'><?php echo $this->Html->image('botez/thumbs/IMG_'.sprintf('%04d', $i).'.jpg', array('alt' => 'Foto Botez')); ?></a>
		<?php } ?>
	<?php } ?>			
</div>
<div class="bicVideo">
    <?php echo $this->Html->image('botez/video.jpg', array('alt' => 'Foto Botez')); ?>
    <a class="bicVideoWrapper" href='/botez_onel.swf?fs=1' rel='lightbox-botez-video'>
        <div class="bicPlayOverlay"></div>
    </a>
</div>
<!-- <div class="bicVideoButton">
	<?php if ($this->params['lang'] == 'eng') { ?>
		<?php echo $this->Html->image('watch_video2.png', array('alt' => 'Video Baptism', 'url' => array('controller' => 'pages', 'action' => 'video_baptism', 'lang' => $this->params['lang']))); ?>
	<?php } else { ?>
		<?php echo $this->Html->image('vizioneaza_video2.png', array('alt' => 'Video Botez', 'url' => array('controller' => 'pages', 'action' => 'video_botez', 'lang' => $this->params['lang']))); ?>
	<?php } ?>
</div> -->

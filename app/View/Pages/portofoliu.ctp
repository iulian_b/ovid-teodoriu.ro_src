<div class="bicPicWrapper">
		<div class="bicPic" id="bicPic_0"><?php echo $this->Html->image('nunta.jpg', array('class' => 'reflect', 'title' => '', 'alt' => '')); ?></div>
		<div class="bicPicMo" id="bicPicMo_0"><?php echo $this->Html->image('nunta-mo.jpg', array('class' => 'reflect', 'title' => '', 'alt' => '', 'url' => array('controller' => 'pages', 'action' => 'nunta', 'lang' => $this->params['lang']))); ?></div>
		<div class="bicPic" id="bicPic_1"><?php echo $this->Html->image('botez.jpg', array('class' => 'reflect', 'title' => '', 'alt' => '')); ?></div>
		<div class="bicPicMo" id="bicPicMo_1"><?php echo $this->Html->image('botez-mo.jpg', array('class' => 'reflect', 'title' => '', 'alt' => '', 'url' => array('controller' => 'pages', 'action' => 'botez', 'lang' => $this->params['lang']))); ?></div>
		<div class="bicPic" id="bicPic_2"><?php echo $this->Html->image('studio.jpg', array('class' => 'reflect', 'title' => '', 'alt' => '')); ?></div>
		<div class="bicPicMo" id="bicPicMo_2"><?php echo $this->Html->image('studio-mo.jpg', array('class' => 'reflect', 'title' => '', 'alt' => '', 'url' => array('controller' => 'pages', 'action' => 'studio', 'lang' => $this->params['lang']))); ?></div>
</div>
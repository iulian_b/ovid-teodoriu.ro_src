<?php echo $this->Html->script('flowplayer-3.2.12.min'); ?>

	<div class="bicLeftColumn">
		<?php if ($this->params['lang'] == 'ro') { ?>
			<?php echo $this->Html->link(__('Back to Photo Gallery'), array('controller' => 'pages', 'action' => 'botez', 'lang' => $this->params['lang'])); ?>
		<?php } else { ?>
			<?php echo $this->Html->link(__('Back to Photo Gallery'), array('controller' => 'pages', 'action' => 'baptism', 'lang' => $this->params['lang'])); ?>
		<?php } ?>
	</div>
	<div class="video_container">
		<div class="bicTitleBox">
			<span class="bicTitleText"><?php echo __('Baptism of little baby girl Andreea Ioana Onel', true); ?></span>
		</div>
		<a
			href="/botez_onel.flv"
			style="display:block;width:600px;height:424px;"
			id="player">
		</a>
		<script language="JavaScript">
			flowplayer("player", "/flowplayer-3.2.16.swf");
		</script>
	</div>
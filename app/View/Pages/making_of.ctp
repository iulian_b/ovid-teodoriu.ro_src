<?php
	echo $this->Html->script('slide_making');
	echo $this->Html->css('../js/fancybox/source/jquery.fancybox');
	echo $this->Html->script('fancybox/source/jquery.fancybox');
	echo $this->Html->script('bicFancybox');
?>
<div class="bicMakingColumn">
	<!--<div class="bicTitleBox" id="bicTopTitle">
		<span class="bicTitleText"><?php echo __('Making of', true); ?></span>
	</div>-->
	<div class="bicBox">
		<div class="bicTitleBox" id="toggleVideo">
			<span class="bicTitleText"><?php echo __('The Video Shoot', true); ?></span>
		</div>
		<div class="bicContentBox" id="bicVideoBox">
			<div class="bicAboutText">
				<div class="bicMakingPhoto">
					<a href="/img/making/filmarea1.jpg" rel="lightbox-making" title="<?php echo __('This gear is also used in cinematography, for dynamic scenes.'); ?>">
						<?php echo $this->Html->image('making/thumbs/filmarea1.jpg'); ?>
					</a>
				</div>
				<span class="bicMakingText">&emsp;&emsp;<?php echo __('The whole event, from the preparations at home until the last dance, will be captured on tape using a professional camera and a image stabilizer. This gear is also used in cinematography, for dynamic scenes.'); ?></span>
			</div>
		</div>
	</div>
	
	<div class="bicBox">
		<div class="bicTitleBox" id="togglePhoto">
			<span class="bicTitleText"><?php echo __('Photography', true); ?></span>
		</div>
		<div class="bicContentBox" id="bicPhotoBox">
			<div class="bicAboutText">
				<div class="bicMakingPhoto">
					<a href="/img/making/fotografierea1.jpg" rel="lightbox-making" title="<?php echo __('The portable studio makes it piece of cake for you to have the studio session of photos right at the wedding place.'); ?>">
						<?php echo $this->Html->image('making/thumbs/fotografierea1.jpg'); ?>
					</a>
				</div>
				<div class="bicMakingText">
					<p>&emsp;&emsp;<?php echo __('Photos will be shot using two formats: JPG - that can be later processed to photos on paper by the client - and a slide-show using the same photos, transitions and a musical background. The best shots, 7 to 15 photos, will be processed to get a more artistic look ( black and white or other visual effects). You will get many photos, at least 700 going up to 2000, depending on the event. For weddings,  the photo-shooting session in the parc will continue at the restaurant, on the dance floor, using a mini-studio that can be assembled and disassembled in a matter of minutes.'); ?></p>
					<p>&emsp;&emsp;<?php echo __('During the party, photos will be taken using four flashes placed in the corners of the room and activated by a radio signal. Good light will increase the image quality.'); ?></p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="bicBox">
		<div class="bicTitleBox" id="toggleScreen">
			<span class="bicTitleText"><?php echo __('On-screen Video Projection', true); ?></span>
		</div>
		<div class="bicContentBox" id="bicScreenBox">
			<div class="bicAboutText">
				<div class="bicMakingPhoto">
					<a href="/img/making/screen1.jpg" rel="lightbox-making" title="<?php echo __('I\'m using the video projection at events for about four years, and it\'s always been a success, because the screen has become the main point of interest.'); ?>">
						<?php echo $this->Html->image('making/thumbs/screen1.jpg'); ?>
					</a>
				</div>
				<div class="bicMakingText">
					<p>&emsp;&emsp;<?php echo __('I\'m using the video projection at events for about four years, and it\'s always been a success, because the screen has become the main point of interest. Everything that the video camera "sees" is projected in realtime on a big screen, using radio remote control. At times, when the music stops, there will be projected photos taken earlier that day, or a video-montage about the bride and the groom.'); ?></p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="bicBox">
		<div class="bicTitleBox" id="togglePost">
			<span class="bicTitleText"><?php echo __('Post-processing &amp; Montage', true); ?></span>
		</div>
		<div class="bicContentBox" id="bicPostBox">
			<div class="bicAboutText">
				<div class="bicMakingPhoto">
					<a href="/img/making/montaj1.jpg" rel="lightbox-making" title="<?php echo __('Video editing is a laborious process, and it can take up to two weeks.'); ?>">
						<?php echo $this->Html->image('making/thumbs/montaj1.jpg'); ?>
					</a>
				</div>
				<div class="bicMakingText">
					<p>&emsp;&emsp;<?php echo __('Video editing is a laborious process, and it can take up to two weeks. The final video contains:'); ?></p>
					<ul>
						<li><?php echo __('a video introduction about the celebrated, with names and locations for the religious ceremony and the party;'); ?></li>
						<li><?php echo __('between musical programs there will be inserted photos of the grooms on artistic backgrounds (flowers or lanscapes) with some best wishes text;'); ?></li>
						<li><?php echo __('at times, the montage contains text to explain and support the visual message;'); ?></li>
						<li><?php echo __('at moments when the original sound is not important, it will be replaced by a suitable musical background.'); ?></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<div class="bicBox">
		<div class="bicTitleBox" id="toggleDvd">
			<span class="bicTitleText"><?php echo __('Delivering The DVDs', true); ?></span>
		</div>
		<div class="bicContentBox" id="bicDvdBox">
			<div class="bicAboutText">
				<div class="bicMakingPhoto">
					<a href="/img/making/dvduri1.jpg" rel="lightbox-making" title="<?php echo __('The final DVDs and the DVD case are personalized.'); ?>">
						<?php echo $this->Html->image('making/thumbs/dvduri1.jpg'); ?>
					</a>
				</div>
				<div class="bicMakingText">
					<p>&emsp;&emsp;<?php echo __('The final DVDs and the DVD case are personalized, meaning that they are printed with an image of the grooms and other photos taken at the event you\'re about to watch. When started, the DVD shows two menus: the main menu, for viewing the entire event, and another one, that lets you pick one particular chapter.'); ?></p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="bicBox">
		<div class="bicTitleBox" id="toggleArchive">
			<span class="bicTitleText"><?php echo __('Safe & Protected Archive', true); ?></span>
		</div>
		<div class="bicContentBox" id="bicArchiveBox">
			<div class="bicAboutText">
				<div class="bicMakingPhoto">
					<a href="/img/making/arhiva1.jpg" rel="lightbox-making" title="<?php echo __('I\'m keeping an archive to help in case you need another copy of the final video package.'); ?>">
						<?php echo $this->Html->image('making/thumbs/arhiva1.jpg'); ?>
					</a>
				</div>
				<div class="bicMakingText">
					<p>&emsp;&emsp;<?php echo __('Because in time discs can deteriorate or even be lost, to prevent the loss I\'m keeping an archive to help in case you need another copy of the final video package.'); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>

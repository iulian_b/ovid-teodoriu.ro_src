<div id="bicStudioList">
        <?php echo $this->Html->link('<div class="bicStudioItem bicStudioDiana"></div>', array('controller' => 'pages', 'action' => 'studio_diana', 'lang' => $this->params['lang']), array('id' => 'dianaWrapper', 'class' => 'bicMetroCell metroCell-2-2', 'escape' => false)); ?>
	
        <?php echo $this->Html->link('<div class="bicStudioItem bicStudioDeea"></div>', array('controller' => 'pages', 'action' => 'studio_deea_bw', 'lang' => $this->params['lang']), array('id' => 'deeaWrapper', 'class' => 'bicMetroCell metroCell-2-1', 'escape' => false)); ?>

        <?php echo $this->Html->link('<div class="bicStudioItem bicStudioMiha"></div>', array('controller' => 'pages', 'action' => 'studio_miha', 'lang' => $this->params['lang']), array('id' => 'mihaWrapper', 'class' => 'bicMetroCell metroCell-1-1', 'escape' => false)); ?>

        <?php echo $this->Html->link('<div class="bicStudioItem bicStudioDiana2"></div>', array('controller' => 'pages', 'action' => 'studio_diana2', 'lang' => $this->params['lang']), array('id' => 'diana2Wrapper', 'class' => 'bicMetroCell metroCell-1-1', 'escape' => false)); ?>

    <?php echo $this->Html->link('<div class="bicStudioItem bicStudioAnda"></div>', array('controller' => 'pages', 'action' => 'studio_anda', 'lang' => $this->params['lang']), array('id' => 'andaWrapper', 'class' => 'bicMetroCell metroCell-1-1', 'escape' => false)); ?>

</div>

<div id="columnsWrapper">
	<div class="bicLeftColumn">
		<div id="bicPhoto">
			<?php echo $this->Html->image('avatar_empty.png'); ?>
			<?php echo $this->Html->image('avatar.png'); ?>
		</div>
	</div>
	<div class="bicRightColumn">
		<div class="bicBox">
			<div class="bicTitleBox" id="bicTopTitle">
				<span class="bicTitleText"><?php echo __('Contact'); ?></span>
			</div>
			<div id="bicContactBox">
				<div class="bicAboutText">
					<div class="bicWrapContactText">
						<p><strong>Ovid Teodoriu</strong> <?php echo __('cameraman &amp; photographer'); ?></p>
						<p><strong><?php echo __('Phone:') ?></strong> 0723 768 588</p>
						<p><strong>Email:</strong> ovid_teodoriu@yahoo.com</p>
						<p><strong>Website:</strong> ovid-teodoriu.ro</p>
						<p>&nbsp;</p>
						<p><strong><?php echo __('For reservations, questions, suggestions or complains, don\'t hesitate to contact me:'); ?></strong><p>
					</div>
					<?php echo $this->Form->create('Contact', array('class' => 'bicContactForm')); ?>
					
						<div class="bicContactRow required">
							<label for="ContactEmail">Email</label>
							<?php echo $this->Form->input('email', array('label' => false)); ?>
						</div>

						<div class="bicContactRow required">
							<label for="ContactName"><?php echo __('Name'); ?></label>
							<?php echo $this->Form->input('name', array('label' => false)); ?>
						</div>

						<div class="bicContactRow">
							<label for="ContactPhone"><?php echo __('Phone'); ?></label>
							<?php echo $this->Form->input('phone', array('label' => false)); ?>
						</div>

						<div class="bicContactRow required">
							<label for="ContactTitle"><?php echo __('Subject'); ?></label>
							<?php echo $this->Form->input('subject', array('label' => false)); ?>
						</div>

						<div class="bicContactRow">
							<label for="ContactMessage"><?php echo __('Message'); ?></label>
							<?php echo $this->Form->input('message', array('label' => false)); ?>
						</div>

						<div class="bicContactRow">
							<label></label>
							<?php echo $this->Form->end(__('Send')); ?>
						</div>
				</div>
			</div>
		</div>

	</div>
</div>

<?php echo $this->Html->script('flowplayer-3.2.12.min'); ?>

	<div class="bicLeftColumn">
		<?php if ($this->params['lang'] == 'ro') { ?>
			<?php echo $this->Html->link(__('Back to Photo Gallery'), array('controller' => 'pages', 'action' => 'nunta', 'lang' => $this->params['lang'])); ?>
		<?php } else { ?>
			<?php echo $this->Html->link(__('Back to Photo Gallery'), array('controller' => 'pages', 'action' => 'wedding', 'lang' => $this->params['lang'])); ?>
		<?php } ?>
	</div>
	<div class="video_container">
		<div class="bicTitleBox">
			<span class="bicTitleText"><?php echo __('Wedding of newlyweds Gabriela &amp; Romeo Fetita', true); ?></span>
		</div>
		<a
			href="/nunta_fetita.flv"
			style="display:block;width:600px;height:424px;"
			id="player">
		</a>
		<script>
			flowplayer("player", "/flowplayer-3.2.16.swf");
		</script>
	</div>
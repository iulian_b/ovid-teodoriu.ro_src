<div id="bicHome">
    <!--  1. NUNTA  -->
    <div class="bicMetroBox">
        <?php echo $this->Html->link('<div class="bicStudioItem bicNunta1"></div>', array('controller' => 'pages', 'action' => __('wedding'), 'lang' => $this->params['lang']), array('class' => 'bicMetroCell metroCell-2-1', 'escape' => false)); ?>

        <?php echo $this->Html->link('<div class="bicStudioItem bicNunta2"></div>', array('controller' => 'pages', 'action' => __('wedding'), 'lang' => $this->params['lang']), array('class' => 'bicMetroCell metroCell-1-1', 'escape' => false)); ?>

        <?php echo $this->Html->link('<div class="bicStudioItem bicNunta3"></div>', array('controller' => 'pages', 'action' => __('wedding'), 'lang' => $this->params['lang']), array('class' => 'bicMetroCell metroCell-1-1', 'escape' => false)); ?>
    </div>


    <!--  2. BOTEZ  -->
    <div class="bicMetroBox">
        <div class="bicMetroColumn bicMetroColumn_left">
            <?php echo $this->Html->link('<div class="bicStudioItem bicBotez1"></div>', array('controller' => 'pages', 'action' => __('baptism'), 'lang' => $this->params['lang']), array('class' => 'bicMetroCell metroCell-1-1', 'escape' => false)); ?>

            <?php echo $this->Html->link('<div class="bicStudioItem bicBotez2"></div>', array('controller' => 'pages', 'action' => __('baptism'), 'lang' => $this->params['lang']), array('class' => 'bicMetroCell metroCell-1-1', 'escape' => false)); ?>
        </div>
        <div class="bicMetroColumn bicMetroColumn_right">
            <?php echo $this->Html->link('<div class="bicStudioItem bicBotez3"></div>', array('controller' => 'pages', 'action' => __('baptism'), 'lang' => $this->params['lang']), array('class' => 'bicMetroCell metroCell-1-2', 'escape' => false)); ?>
        </div>
    </div>


    <!-- 3. NATURA  -->
    <div class="bicMetroBox">
        <?php echo $this->Html->link('<div class="bicStudioItem bicNature3"></div>', array('controller' => 'pages', 'action' => __('nature'), 'lang' => $this->params['lang']), array('class' => 'bicMetroCell metroCell-1-1', 'escape' => false)); ?>

        <?php echo $this->Html->link('<div class="bicStudioItem bicNature2"></div>', array('controller' => 'pages', 'action' => __('nature'), 'lang' => $this->params['lang']), array('class' => 'bicMetroCell metroCell-1-1', 'escape' => false)); ?>

        <?php echo $this->Html->link('<div class="bicStudioItem bicNature1"></div>', array('controller' => 'pages', 'action' => __('nature'), 'lang' => $this->params['lang']), array('class' => 'bicMetroCell metroCell-1-1', 'escape' => false)); ?>

        <?php echo $this->Html->link('<div class="bicStudioItem bicNature4"></div>', array('controller' => 'pages', 'action' => __('nature'), 'lang' => $this->params['lang']), array('class' => 'bicMetroCell metroCell-1-1', 'escape' => false)); ?>
    </div>


    <!-- 4. STUDIO  -->
    <div class="bicMetroBox">
        <?php echo $this->Html->link('<div class="bicStudioItem bicStudioDiana"></div>', array('controller' => 'pages', 'action' => 'studio_diana', 'lang' => $this->params['lang']), array('class' => 'bicMetroCell metroCell-1-1', 'escape' => false)); ?>

        <?php echo $this->Html->link('<div class="bicStudioItem bicStudioDeea"></div>', array('controller' => 'pages', 'action' => 'studio_deea_bw', 'lang' => $this->params['lang']), array('class' => 'bicMetroCell metroCell-1-1', 'escape' => false)); ?>

        <?php echo $this->Html->link('<div class="bicStudioItem bicStudioMiha"></div>', array('controller' => 'pages', 'action' => 'studio_miha', 'lang' => $this->params['lang']), array('class' => 'bicMetroCell metroCell-2-1', 'escape' => false)); ?>
    </div>

</div>

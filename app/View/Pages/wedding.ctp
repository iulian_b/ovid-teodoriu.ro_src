<div id="bicGalleryWrapper">
	<?php for($i=0; $i < 2000; $i++) { ?>
		<?php if (file_exists(WWW_ROOT.'img/nunta/thumbs/IMG_'.sprintf('%04d', $i).'.jpg')) {?>
			<a class="bicImageWrapper" href='<?php echo '/img/nunta/IMG_'.sprintf('%04d', $i).'.jpg'; ?>' rel='lightbox-nunta'><?php echo $this->Html->image('nunta/thumbs/IMG_'.sprintf('%04d', $i).'.jpg', array('alt' => 'Foto Nunta')); ?></a>
		<?php } ?>
	<?php } ?>			
</div>

<div class="bicVideo">
    <?php echo $this->Html->image('nunta/video.jpg', array('alt' => 'Foto Nunta')); ?>
    <a class="bicVideoWrapper" href='/nunta_fetita.swf?fs=1' rel='lightbox-nunta-video'>
        <div class="bicPlayOverlay"></div>
    </a>
</div>
<!-- <div class="bicVideoButton">
	<?php if ($this->params['lang'] == 'eng') { ?>
		<?php echo $this->Html->image('watch_video2.png', array('alt' => 'Video Wedding', 'url' => array('controller' => 'pages', 'action' => 'video_wedding', 'lang' => $this->params['lang']))); ?>
	<?php } else { ?>
		<?php echo $this->Html->image('vizioneaza_video2.png', array('alt' => 'Video Nunta', 'url' => array('controller' => 'pages', 'action' => 'video_nunta', 'lang' => $this->params['lang']))); ?>
	<?php } ?>
</div> -->

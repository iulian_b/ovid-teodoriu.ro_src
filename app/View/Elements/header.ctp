<div class="bicLanguages">
	<?php echo $this->Html->image('flag_ro.png', array('url' => array('controller' => 'pages'), 'title' => 'Tradu in Romana')); ?>
	<?php echo $this->Html->image('flag_brit.png', array('url' => array('lang' => 'eng'), 'title' => 'Translate to English')); ?>
</div>
<a href="/<?php echo $this->params['lang']; ?>"><?php echo $this->Html->image('logo_white.png', array('title' => 'Logo Ovid Teodoriu', 'width' => '343', 'height' => '125', 'class' => 'bicLogo')); ?></a>
<!-- <div class="bicPhoneWrapper">
	<span class="bicPhone"><?php echo __('Phone Ovid'); ?>: 0723 768 588</span>
</div> -->

<div class="bicFacebook">
	<a href="http://www.facebook.com/teodoriu.ovid" target="__blank"><?php echo $this->Html->image('facebook-icon2.png', array('title' => __('Ovid on Facebook', true))); ?></a>
</div>

<div id="bicBurgerMenu"></div>
<div class="bicMeniu">
	<ul id="bicMeniuNav">
        <li class="<?php if ($this->params['action']==__('about')) echo 'bicActivePage'; ?>"><?php echo $this->Html->link(__('About Me', true), array('controller' => 'pages', 'action' => __('about'), 'lang' => $this->params['lang'])); ?></li>
        <li class="<?php if ($this->params['action']==__('wedding')) echo 'bicActivePage'; ?>"><?php echo $this->Html->link(__('Wedding'), array('controller' => 'pages', 'action' => __('wedding'), 'lang' => $this->params['lang'])); ?></li>
        <li class="<?php if ($this->params['action']==__('baptism')) echo 'bicActivePage'; ?>"><?php echo $this->Html->link(__('Baptism'), array('controller' => 'pages', 'action' => __('baptism'), 'lang' => $this->params['lang'])); ?></li>
        <li class="<?php if (substr($this->params['action'], 0, strlen('studio')) == 'studio') echo 'bicActivePage'; ?>"><?php echo $this->Html->link(__('Studio'), array('controller' => 'pages', 'action' => __('studio_list'), 'lang' => $this->params['lang'])); ?></li>
        <li class="<?php if ($this->params['action']==__('nature')) echo 'bicActivePage'; ?>"><?php echo $this->Html->link(__('Nature', true), array('controller' => 'pages', 'action' => __('nature'), 'lang' => $this->params['lang'])); ?></li>
        <li class="<?php if ($this->params['action']==__('making_of')) echo 'bicActivePage'; ?>"><?php echo $this->Html->link(__('Making of'), array('controller' => 'pages', 'action' => __('making_of'), 'lang' => $this->params['lang'])); ?></li>
        <li class="<?php if ($this->params['action']==__('contact')) echo 'bicActivePage'; ?>"><?php echo $this->Html->link(__('Contact'), array('controller' => 'pages', 'action' => __('contact'), 'lang' => $this->params['lang'])); ?></li>
	</ul>
</div>

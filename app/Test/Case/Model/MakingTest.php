<?php
/* Making Test cases generated on: 2012-01-14 01:45:01 : 1326498301*/
App::uses('Making', 'Model');

/**
 * Making Test Case
 *
 */
class MakingTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.making');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Making = ClassRegistry::init('Making');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Making);

		parent::tearDown();
	}

}

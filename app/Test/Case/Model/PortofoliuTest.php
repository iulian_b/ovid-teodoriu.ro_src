<?php
/* Portofoliu Test cases generated on: 2012-01-14 01:33:30 : 1326497610*/
App::uses('Portofoliu', 'Model');

/**
 * Portofoliu Test Case
 *
 */
class PortofoliuTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.portofoliu');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Portofoliu = ClassRegistry::init('Portofoliu');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Portofoliu);

		parent::tearDown();
	}

}

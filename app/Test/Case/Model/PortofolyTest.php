<?php
/* Portofoly Test cases generated on: 2012-01-14 01:36:18 : 1326497778*/
App::uses('Portofoly', 'Model');

/**
 * Portofoly Test Case
 *
 */
class PortofolyTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.portofoly');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Portofoly = ClassRegistry::init('Portofoly');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Portofoly);

		parent::tearDown();
	}

}

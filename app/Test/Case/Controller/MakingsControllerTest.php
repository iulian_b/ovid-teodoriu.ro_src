<?php
/* Makings Test cases generated on: 2012-01-14 01:46:32 : 1326498392*/
App::uses('MakingsController', 'Controller');

/**
 * TestMakingsController *
 */
class TestMakingsController extends MakingsController {
/**
 * Auto render
 *
 * @var boolean
 */
	public $autoRender = false;

/**
 * Redirect action
 *
 * @param mixed $url
 * @param mixed $status
 * @param boolean $exit
 * @return void
 */
	public function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

/**
 * MakingsController Test Case
 *
 */
class MakingsControllerTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.making');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Makings = new TestMakingsController();
		$this->Makings->constructClasses();
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Makings);

		parent::tearDown();
	}

}
